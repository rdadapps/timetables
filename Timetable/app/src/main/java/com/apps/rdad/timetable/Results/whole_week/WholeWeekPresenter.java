package com.apps.rdad.timetable.Results.whole_week;

import com.apps.rdad.timetable.database.Result;

import java.util.ArrayList;

/**
 * Created by Yoga on 2017-02-05.
 */
public class WholeWeekPresenter {

    WholeWeekContract.InnerViewPager mInnerViewPager;

    public WholeWeekPresenter(WholeWeekContract.InnerViewPager innerViewPager) {
        this.mInnerViewPager = innerViewPager;
    }


    public void initView(ArrayList<Result> results, boolean isWholeDay) {
        if(results==null || results.isEmpty()){
            mInnerViewPager.setEmptyList();
        }else {
            if(isWholeDay){
                mInnerViewPager.initWholeDayAdapter(results);
            }else {
                mInnerViewPager.initAdapter(results);
            }
        }
    }
}
