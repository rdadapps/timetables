package com.apps.rdad.timetable.Results;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.database.Result;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yoga on 2016-10-03.
 */

public class WholeDayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Result> mTimetableList;
    Context context;

    @Override
    public long getItemId(int i) {
        return 0;
    }


    public WholeDayAdapter() {
        mTimetableList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.item_result_whole_day, parent, false);
        return new WholeDayAdapter.ViewHolder (view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int color = 0;
        ((WholeDayAdapter.ViewHolder)holder).mTime.setText(mTimetableList.get(position).getDepartureTime());

        switch(mTimetableList.get(position).getCompany()){
            case "Globus": {
                color = R.color.colorGlobus;
                break;
            }
            case "GołbaBus":{
                color = R.color.colorGolbaBus;
                break;
            }
            case "Makbus": {
                color = R.color.colorMakbus;
                break;
            }
            case "Bako":{
                color = R.color.colorBako;
                break;
            }
        }

        ((WholeDayAdapter.ViewHolder)holder).mTime.setBackgroundColor(context.getResources().getColor(color));
    }

    @Override
    public int getItemCount() {
        return this.mTimetableList.size();
    }

    public void setResult(List<Result> result) {
        mTimetableList.addAll(result);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tt_departure_time)
        TextView mTime;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}

