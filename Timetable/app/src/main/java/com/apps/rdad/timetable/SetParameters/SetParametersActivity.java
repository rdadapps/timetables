package com.apps.rdad.timetable.SetParameters;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.Results.ResultsActivity;
import com.apps.rdad.timetable.Results.whole_week.WholeWeekResultsActivity;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class SetParametersActivity extends AppCompatActivity {

    public static String ROUTE_PARAMS = "route params";

    @BindView(R.id.start_location)
    MaterialBetterSpinner mStartLocation;
    @BindView(R.id.stop_location)
    MaterialBetterSpinner mStopLocation;
    @BindView(R.id.departure_time)
    TextView mDepartureTime;
    @BindView(R.id.weekday)
    MaterialBetterSpinner mWeekday;
    @BindView(R.id.carrier)
    MaterialBetterSpinner mCompany;
    @BindView(R.id.search_btn)
    Button mSearchBtn;
    @BindView(R.id.checkBox)
    CheckBox mCheckBoxWholeDay;

    private SetParametersPresenter mSetParametersPresenter;
    private String[] week_days;
    private String[] companies;
    private String[] stops;
    private ArrayAdapter<String> adapterStops, adapterWeekdays, adapterCompanies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_params);
        stops = getResources().getStringArray(R.array.stops);
        week_days = getResources().getStringArray(R.array.weekdays);
        companies = getResources().getStringArray(R.array.companies);

        ButterKnife.bind(this);
        mSetParametersPresenter = new SetParametersPresenter(this);
        setInitialValues();
    }

    public void setInitialValues(){
        adapterStops = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stops);
        adapterWeekdays = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, week_days);
        adapterCompanies = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, companies);

        mStartLocation.setAdapter(adapterStops);
        mStopLocation.setAdapter(adapterStops);
        mStartLocation.setIconLeft(R.drawable.ray_start_arrow);
        mStopLocation.setIconLeft(R.drawable.ray_start_end);
        mSearchBtn.setEnabled(false);
        mWeekday.setAdapter(adapterWeekdays);
        mCompany.setAdapter(adapterCompanies);
        setOnItemStopCLick(mStartLocation);
        setOnItemStopCLick(mStopLocation);
        setOnItemDayClick();
        setOnItemCompanyClick();
    }
    public void setOnItemStopCLick(final MaterialBetterSpinner spinner){
        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(spinner.equals(mStopLocation)) {
                    mSetParametersPresenter.setEndStop( stops[i]);
                }
                if(spinner.equals(mStartLocation)) {
                    mSetParametersPresenter.setStartStop(stops[i]);
                }
                mSetParametersPresenter.checkStops();

            }
        });

    }

    private void setOnItemCompanyClick() {
        mCompany.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    mSetParametersPresenter.setCompany(companies[i]);

            }
        });
    }

    private void setOnItemDayClick() {
        mWeekday.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mSetParametersPresenter.setDay(week_days[i]);
            }
        });
    }

    @OnClick(R.id.search_btn)
    public void onSearchClick(){
        mSetParametersPresenter.goToResults(week_days[0], companies[0]);
    }


    @OnClick(R.id.departure_time)
    public void onTimeClick() {
        checkWholeDay(false);
        final TimePickerDialog timePickerDialog = new TimePickerDialog(SetParametersActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                mSetParametersPresenter.setTime(String.format("%02d:%02d", hourOfDay, minute));
                mDepartureTime.setText(String.format("%02d:%02d", hourOfDay, minute));
            }
        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
        timePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                timePickerDialog.dismiss();
            }
        });
        timePickerDialog.setTitle(getString(R.string.dialog_choose_departure_time));
        timePickerDialog.show();

    }

    @OnCheckedChanged(R.id.checkBox)
    public void onWholeDayChecked() {

        mDepartureTime.setText("");

        if (mCheckBoxWholeDay.isChecked()){
            mSetParametersPresenter.setTime(getString(R.string.whole_day));
            mDepartureTime.setHint(R.string.whole_day);
        } else {
            mDepartureTime.setHint(R.string.departure_time);
        }
    }



    public void goToResults(RouteParameters routeParameters) {
        Intent intent;
        if(routeParameters.getWeekday().equals(week_days[0])){
            intent = new Intent(SetParametersActivity.this, WholeWeekResultsActivity.class);

        }else {
            intent = new Intent(SetParametersActivity.this, ResultsActivity.class);
        }
        intent.putExtra(ROUTE_PARAMS, routeParameters);
        startActivity(intent);
    }

    public void checkWholeDay(boolean check) {
        mCheckBoxWholeDay.setChecked(check);
    }

    public void setSearchEnabled(boolean searchEnabled) {
        mSearchBtn.setEnabled(searchEnabled);
    }
}
