package com.apps.rdad.timetable.Results.whole_week;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.Results.TimetableListAdapter;
import com.apps.rdad.timetable.Results.VerticalSpaceItemDecorator;
import com.apps.rdad.timetable.Results.WholeDayAdapter;
import com.apps.rdad.timetable.database.Result;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yoga on 2017-01-21.
 */

public class WorkingWeekFragment extends Fragment implements WholeWeekContract.InnerViewPager{

    private static final String TIMETABLE_LIST = "timetable list";

    @BindView(R.id.timetable_rv)
    RecyclerView mTimetableRV;
    @BindView(R.id.no_results)
    TextView mNoResultsTV;

    private TimetableListAdapter adapter;
    private WholeWeekPresenter mPresenter;
    private WholeDayAdapter wdAdapter;

    public static WorkingWeekFragment newInstance(List<Result> timetableList, boolean isWholeDay) {
        WorkingWeekFragment fragment = new WorkingWeekFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(TIMETABLE_LIST, (ArrayList<Result>) timetableList);
        bundle.putBoolean(WholeWeekResultsActivity.IS_WHOLEDAY, isWholeDay);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_working_week, container, false);
        ButterKnife.bind(this, rootView);

        mPresenter = new WholeWeekPresenter(this);
        Bundle args = getArguments();
        ArrayList<Result> results = args.getParcelableArrayList(TIMETABLE_LIST);
        mPresenter.initView(results, args.getBoolean(WholeWeekResultsActivity.IS_WHOLEDAY));

        return rootView;

    }


    public void setEmptyList() {
        mNoResultsTV.setVisibility(View.VISIBLE);
        mTimetableRV.setVisibility(View.GONE);

    }

    public void initAdapter(ArrayList<Result> results) {
        mNoResultsTV.setVisibility(View.GONE);
        mTimetableRV.setVisibility(View.VISIBLE);
        mTimetableRV.setLayoutManager(new LinearLayoutManager(this.getContext()));
        adapter = new TimetableListAdapter();
        mTimetableRV.setAdapter(adapter);
        mTimetableRV.addItemDecoration(new VerticalSpaceItemDecorator(16));
        adapter.setResult(results);

    }

    @Override
    public void initWholeDayAdapter(ArrayList<Result> results) {
        mNoResultsTV.setVisibility(View.GONE);
        mTimetableRV.setVisibility(View.VISIBLE);
        wdAdapter = new WholeDayAdapter();
        mTimetableRV.setLayoutManager(new GridLayoutManager(getContext(),3));
        mTimetableRV.setAdapter(wdAdapter);
        mTimetableRV.addItemDecoration(new VerticalSpaceItemDecorator(16));
        wdAdapter.setResult(results);
    }
}