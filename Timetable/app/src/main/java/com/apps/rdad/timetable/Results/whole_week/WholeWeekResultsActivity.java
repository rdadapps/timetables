package com.apps.rdad.timetable.Results.whole_week;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.Results.ResultsPresenter;
import com.apps.rdad.timetable.SetParameters.RouteParameters;
import com.apps.rdad.timetable.SetParameters.SetParametersActivity;
import com.apps.rdad.timetable.database.DatabaseHelper;
import com.apps.rdad.timetable.database.Result;

import java.text.ParseException;
import java.util.List;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yoga on 2017-01-21.
 */

public class WholeWeekResultsActivity extends AppCompatActivity implements WholeWeekContract.OuterView {
    public static final String IS_WHOLEDAY = "is_whole_day";

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindArray(R.array.weekdays)
    String[] week_days;

    private WholeWeekResultsPresenter mPresenter;
    private List<Result> mTimetableListWorkingWeek;
    private List<Result> mTimetableListSaturday;
    private List<Result> mTimetableListSunday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results_whole_week);
        ButterKnife.bind(this);
        mPresenter = new WholeWeekResultsPresenter(this, new DatabaseHelper(this), this);
        mPresenter.initDB();

        Bundle bundle = getIntent().getExtras();
        RouteParameters routeParameters = bundle.getParcelable(SetParametersActivity.ROUTE_PARAMS);
        initData(routeParameters);
        boolean isWholeDay = routeParameters.getTime().equals("") || routeParameters.getTime().equals(getString(R.string.whole_day_label)) ;
        //if getTime is empty or 'Caly dzien' - whole day is checked. if whole day is checked - getTime is empty

        setupViewPager(viewPager, isWholeDay);
        tabLayout.setupWithViewPager(viewPager);

    }

    public void initData(RouteParameters routeParameters) {
        try {
            mTimetableListWorkingWeek = mPresenter.getTimetableList(week_days[1], routeParameters.getCompany(), routeParameters.getStartStop(),
                    routeParameters.getStopStop(), routeParameters.getTime());
            mTimetableListSaturday = mPresenter.getTimetableList(week_days[2], routeParameters.getCompany(), routeParameters.getStartStop(),
                    routeParameters.getStopStop(), routeParameters.getTime());
            mTimetableListSunday = mPresenter.getTimetableList(week_days[3], routeParameters.getCompany(), routeParameters.getStartStop(),
                    routeParameters.getStopStop(), routeParameters.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setupViewPager(ViewPager viewPager, boolean isWholeDay) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(WorkingWeekFragment.newInstance(mTimetableListWorkingWeek, isWholeDay), getString(R.string.working_week_title));
        adapter.addFragment(SaturdayFragment.newInstance(mTimetableListSaturday, isWholeDay), getString(R.string.saturday_title));
        adapter.addFragment(SundayFragment.newInstance(mTimetableListSunday, isWholeDay), getString(R.string.sunday_title));
        viewPager.setAdapter(adapter);
    }

}