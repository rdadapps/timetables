package com.apps.rdad.timetable.Results;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.SetParameters.RouteParameters;
import com.apps.rdad.timetable.SetParameters.SetParametersActivity;
import com.apps.rdad.timetable.database.DatabaseHelper;
import com.apps.rdad.timetable.database.Result;

import org.zakariya.stickyheaders.StickyHeaderLayoutManager;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yoga on 2016-09-24.
 */
public class ResultsActivity extends AppCompatActivity implements ResultsContract.View{

    @BindView(R.id.results_ll)
    LinearLayout mResultsLL;
    @BindView(R.id.no_results)
    TextView mNoResultsTV;
    @BindView(R.id.timetable_rv)
    RecyclerView mTimetableRV;
    @BindView(R.id.route_tv)
    TextView mRouteTV;
    @BindView(R.id.weekday_tv)
    TextView mWeekdayTV;

    private ResultsPresenter mResultPresenter;
    private DatabaseHelper mDBHelper;
    private String dayOfWeek, company, departTime, startLocation, stopLocation;
    private WholeDayAdapter wholeDayAdapter;
    private TimetableListAdapter mTimetableListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        ButterKnife.bind(this);
        mDBHelper = new DatabaseHelper(this);

        mResultPresenter = new ResultsPresenter(this, mDBHelper, this);

            mResultPresenter.initDB();

        Bundle bundle = getIntent().getExtras();
        RouteParameters routeParameters = bundle.getParcelable(SetParametersActivity.ROUTE_PARAMS);
        dayOfWeek = routeParameters.getWeekday();
        company = routeParameters.getCompany();
        departTime = routeParameters.getTime().equals("") ? getString(R.string.whole_day_label) : routeParameters.getTime();
        startLocation = routeParameters.getStartStop();
        stopLocation = routeParameters.getStopStop();
        mRouteTV.setText(startLocation + " - " + stopLocation);

        try {
            mResultPresenter.getTimetableList(dayOfWeek, company, startLocation, stopLocation, departTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void setEmptyList(){
        mNoResultsTV.setVisibility(View.VISIBLE);
        mResultsLL.setVisibility(View.GONE);

    }

    @Override
    public void initAdapter(ArrayList<Result> results) {
        mNoResultsTV.setVisibility(View.GONE);
        mResultsLL.setVisibility(View.VISIBLE);
        mWeekdayTV.setText(results.get(0).getWeekDays());

        mTimetableRV.setLayoutManager(new LinearLayoutManager(this));
        mTimetableListAdapter = new TimetableListAdapter();
        mTimetableRV.setAdapter(mTimetableListAdapter);
        mTimetableRV.addItemDecoration(new VerticalSpaceItemDecorator(16));
        if(results==null)
            return;

        if(!results.isEmpty())
            mTimetableListAdapter.setResult(results);
        else
            Toast.makeText(this, R.string.error_no_route, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void initWholeDayAdapter(ArrayList<Result> results) {
        mNoResultsTV.setVisibility(View.GONE);
        mResultsLL.setVisibility(View.VISIBLE);
        mWeekdayTV.setText(results.get(0).getWeekDays());
        mTimetableRV.setLayoutManager( new GridLayoutManager(getApplicationContext(),3));
        wholeDayAdapter = new WholeDayAdapter();
        mTimetableRV.setAdapter(wholeDayAdapter);
        mTimetableRV.addItemDecoration(new VerticalSpaceItemDecorator(16));
        if(results==null)
            return;

        if(!results.isEmpty())
            wholeDayAdapter.setResult(results);
        else
            Toast.makeText(this, R.string.error_no_route, Toast.LENGTH_SHORT).show();
    }
}
