package com.apps.rdad.timetable.database;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yoga on 2016-10-03.
 */
public class Result implements Parcelable{

    private int mId;
    private String mRoute;
    private String mDepartureTime;
    private String mArriveTime;
    private String mCompany;
    private String mWeekDays;

    public Result(int id, String route, String departureTime, String arriveTime, String company, String weekDays) {
        this.mId = id;
        this.mRoute = route;
        this.mArriveTime = arriveTime;
        this.mDepartureTime = departureTime;
        this.mCompany = company;
        this.mWeekDays = weekDays;
    }

    protected Result(Parcel in) {
        mId = in.readInt();
        mRoute = in.readString();
        mDepartureTime = in.readString();
        mArriveTime = in.readString();
        mCompany = in.readString();
        mWeekDays = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getDepartureTime() {
        return mDepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        mDepartureTime = departureTime;
    }

    public String getRoute() {
        return mRoute;
    }

    public void setRoute(String route) {
        mRoute = route;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getWeekDays() {
        return mWeekDays;
    }

    public void setWeekDays(String weekDays) {
        mWeekDays = weekDays;
    }

    public String getArriveTime() {
        return mArriveTime;
    }

    public void setArriveTime(String arriveTime) {
        mArriveTime = arriveTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mId);
        parcel.writeString(mRoute);
        parcel.writeString(mDepartureTime);
        parcel.writeString(mArriveTime);
        parcel.writeString(mCompany);
        parcel.writeString(mWeekDays);
    }
}
