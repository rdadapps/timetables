package com.apps.rdad.timetable;

import android.app.Application;
import android.content.Context;

/**
 * Created by Yoga on 2016-10-03.
 */

public class TimetableApplication extends Application {

    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        TimetableApplication.mContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return TimetableApplication.mContext;
    }
}