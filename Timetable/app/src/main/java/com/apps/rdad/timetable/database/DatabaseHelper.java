package com.apps.rdad.timetable.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.TimetableApplication;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by Yoga on 2016-10-03.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private SQLiteDatabase mDatabase;

    public DatabaseHelper(Context context) {
        super(context, DatabaseConstants.DB_NAME, null, 1);
    }

    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (dbExist) {
            //do nothing - database already exist
        } else {
            this.getReadableDatabase();

            try {
                copyDataBase();
            } catch (IOException e) {

                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDataBase() {

        SQLiteDatabase checkDB = null;

        try {
            checkDB = SQLiteDatabase.openDatabase(DatabaseConstants.DB_PATH, null, SQLiteDatabase.OPEN_READONLY);

        } catch (SQLiteException e) {
        }

        if (checkDB != null) {
            checkDB.close();

        }
        return checkDB != null ? true : false;
    }

    /**
     * Copies your database from your local assets-folder to the just created empty database in the
     * system folder, from where it can be accessed and handled.
     * This is done by transfering bytestream.
     */
    private void copyDataBase() throws IOException {

        //Open your local db as the input stream
        InputStream myInput = TimetableApplication.getAppContext().getAssets().open(DatabaseConstants.DB_NAME);

        // Path to the just created empty db

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(DatabaseConstants.DB_PATH);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public void openDataBase() throws SQLException {

        mDatabase = SQLiteDatabase.openDatabase(DatabaseConstants.DB_PATH, null, SQLiteDatabase.OPEN_READONLY);

    }

    @Override
    public synchronized void close() {

        if (mDatabase != null)
            mDatabase.close();

        super.close();

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }


    public List<Integer> mPositionStart = new ArrayList<Integer>();
    public List<Integer>  mPositionStop = new ArrayList<Integer>(); 
    public int mDirectionChanged;
    public List<String> mMatchingRoutesList = new ArrayList<String>();
    public List<String> mMatchingRoutesList_new = new ArrayList<String>();
    public String mMatchingRoutesStr;

    public List<Result> getTimetableList(String daysOfWeek, String strCompany, String startLoc, String stopLoc, String myDepartTime) throws ParseException {

        List<Result> timetableList = new ArrayList<>();
        List<Result> timetableListUpdated = new ArrayList<>();
        int minutesStart = 0, minutesStop = 0;
        List<Result> matchingList;
        Map<Result, Long> matching = new HashMap<>();

        mMatchingRoutesStr = returnMatchingRoutes(startLoc, stopLoc);

        if(mMatchingRoutesStr.length()!=0) {
            mMatchingRoutesStr = mMatchingRoutesStr.substring(0, mMatchingRoutesStr.length()-1);

            timetableList = getRawList(daysOfWeek, strCompany);
        }

        for (Result result: timetableList) {
            minutesStart = getTimeForStop(result, startLoc);
            minutesStop = getTimeForStop(result, stopLoc);

            result.setDepartureTime(addTime(result.getDepartureTime(), minutesStart));
            result.setArriveTime(addTime(result.getArriveTime(), minutesStop));

            if (!myDepartTime.equals(TimetableApplication.getAppContext().getResources().getString(R.string.whole_day_label)) && !myDepartTime.equals("")) {
                result.setRoute(String.valueOf(subOperation(result.getDepartureTime())));
                if (subOperation(result.getDepartureTime(), myDepartTime) >= 0 && subOperation(result.getDepartureTime(), myDepartTime) < 120) {

                    timetableListUpdated.add(result);
                    matching.put(result, subOperation(result.getDepartureTime(), myDepartTime));
                }
            }
            else if(myDepartTime.equals(TimetableApplication.getAppContext().getResources().getString(R.string.whole_day_label))){
                matching.put(result, subOperation(result.getDepartureTime(), "00:00"));

            }
        }
        matchingList = sortByValue(matching);

        return matchingList;
    }

    private String addTime(String time, int minutesToAdd) throws ParseException {

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Date date = df.parse(time);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minutesToAdd);
        String newTime = df.format(cal.getTime());

        return newTime;
    }

    private long subOperation(String time1, String time2) throws ParseException {
        String[] temp1 = time1.split(":");
        String[] temp2 = time2.split(":");

        Calendar fromDate1 = Calendar.getInstance();
        Calendar fromDate2 = Calendar.getInstance();

        fromDate1.setTimeZone(TimeZone.getTimeZone("GMT+01:00"));
        fromDate2.setTimeZone(TimeZone.getTimeZone("GMT+01:00"));

        fromDate1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(temp1[0]));
        fromDate1.set(Calendar.MINUTE, Integer.parseInt(temp1[1]));
        fromDate2.set(Calendar.HOUR_OF_DAY, Integer.parseInt(temp2[0]));
        fromDate2.set(Calendar.MINUTE, Integer.parseInt(temp2[1]));

        long subRes =(fromDate1.getTimeInMillis() - fromDate2.getTimeInMillis())/60000;

        return subRes;
    }

    private long subOperation(String time1) throws ParseException {
        String[] temp1 = time1.split(":");

        Calendar fromDate1 = Calendar.getInstance();
        Calendar fromDate2 = Calendar.getInstance();

        fromDate1.setTimeZone(TimeZone.getTimeZone("GMT+01:00"));
        fromDate2.setTimeZone(TimeZone.getTimeZone("GMT+01:00"));

        fromDate1.set(Calendar.HOUR_OF_DAY, Integer.parseInt(temp1[0]));
        fromDate1.set(Calendar.MINUTE, Integer.parseInt(temp1[1]));

        long subRes =(fromDate1.getTimeInMillis() - fromDate2.getTimeInMillis())/60000;

        Log.e("SPR ",fromDate1.toString() + " " + fromDate2.toString());

        return subRes;
    }


    public List<Result> getRawList(String daysOfWeek, String strCompany){
        List<Result> rawList = new ArrayList<>();

        String strQuery = "SELECT * FROM AllData WHERE " + DatabaseConstants.ALLDATA_KEY_ROUTE + " IN (" + mMatchingRoutesStr + ")";

        if (!daysOfWeek.equals(DatabaseConstants.PARAM_ALL_WEEK)) {
            strQuery += " AND " + DatabaseConstants.ALLDATA_KEY_WEEKDAY + "='" + daysOfWeek + "'";
        }
        if (!strCompany.equals(DatabaseConstants.PARAM_ALL_COMPANIES)) {
            strQuery += " AND " + DatabaseConstants.ALLDATA_KEY_COMPANY + "='" + strCompany + "'";
        }
        Cursor cursor = mDatabase.rawQuery(strQuery, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            rawList.add(new Result(cursor.getInt(0), cursor.getString(1),
                    cursor.getString(2), cursor.getString(2), cursor.getString(3), cursor.getString(4)));
            cursor.moveToNext();
        }
        cursor.close();
    return rawList;
    }


    public String returnMatchingRoutes(String myStart, String myStop) {
        String matchingRoutes = "";

        Cursor cursor = mDatabase.rawQuery("SELECT " + DatabaseConstants.ROUTES_KEY_ROUTENAME + ", " + DatabaseConstants.ROUTES_KEY_BUSSTOPS + " FROM " + DatabaseConstants.ROUTES_TABLE, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            if (verifyRouteDirection(cursor.getString(0), cursor.getString(1).split(",").length, myStart, myStop) != null){
                matchingRoutes += "'" + verifyRouteDirection(cursor.getString(0), cursor.getString(1).split(",").length, myStart, myStop) + "',";
            }
            cursor.moveToNext();
        }
        cursor.close();

        return matchingRoutes;
    }


    public String verifyRouteDirection(String route, int maxStops, String myStart, String myStop) {
        String result;

        int positionStart = busStopPositionInRoute(route, myStart);
        int positionStop = busStopPositionInRoute(route, myStop);
        result = route;

        if (positionStart == -1 || positionStop == -1) {
            result = null;
        }else {
                mMatchingRoutesList.add(route);
            if (positionStop > positionStart) {
                    mPositionStart.add(positionStart);
                    mPositionStop.add(positionStop);
                    mDirectionChanged = 0; //false
            }
            else {
                positionStart = maxStops - 1 - positionStart;
                positionStop = maxStops - 1 - positionStop;
                mPositionStart.add(positionStart);
                mPositionStop.add(positionStop);
                mDirectionChanged = 1; //true
                String[] temp = route.split("-");
                result = temp[1] + "-" + temp[0];
            }
        }
        return result;
    }


    public int busStopPositionInRoute(String checkingRoute, String busStop) {
        String[] result;
        int busStopPosition = -1;
        String strQuery  = "SELECT * FROM " + DatabaseConstants.ROUTES_TABLE + " WHERE RouteName='" + checkingRoute + "'";

        Cursor cursor = mDatabase.rawQuery(strQuery, null);
        cursor.moveToFirst();
            result = (cursor.getString(2)).split(",");
            cursor.close();

            for (int i = 0; i < result.length; i++) {
                if (busStop.equals(result[i]))
                    busStopPosition = i;

            }

        return busStopPosition;
    }

    public List<MinutesToDepart> getTimeForStops(String company){
        List<MinutesToDepart> minutesToDepart = new ArrayList<>();
        String[] allTimes;
        String strQuery = "SELECT "+DatabaseConstants.TIMETOADD_KEY_ROUTESTOPSTIME + ", " + DatabaseConstants.TIMETOADD_KEY_COMPANY +
                " FROM " + DatabaseConstants.TIMETOADD_TABLE + " INNER JOIN " + DatabaseConstants.ROUTES_TABLE + " ON " +
                DatabaseConstants.TIMETOADD_KEY_ROUTEID + "=" + DatabaseConstants.ROUTES_KEY_ROUTEID + " WHERE " + DatabaseConstants.TIMETOADD_KEY_ROUTEDIRECTION + "=" + mDirectionChanged;

        if (!company.equals(DatabaseConstants.PARAM_ALL_COMPANIES)) {
            strQuery += " AND " + DatabaseConstants.TIMETOADD_KEY_COMPANY + "='" + company + "'";
        }
        Cursor cursor = mDatabase.rawQuery(strQuery, null);

        cursor.moveToFirst();

        int i=0;
        while (!cursor.isAfterLast()) {
            allTimes = cursor.getString(0).split(",");

            int addToStartTime = Integer.parseInt(allTimes[mPositionStart.get(i)]);

            int addToStopTime = Integer.parseInt(allTimes[mPositionStop.get(i)]);

            minutesToDepart.add(new MinutesToDepart(addToStartTime, addToStopTime, cursor.getString(1)));

            i++;
            cursor.moveToNext();
        }

        cursor.close();
        return minutesToDepart;
    }


    public int getTimeForStop(Result result, String stop){
        int addToStartTime = 0;
        String[] allTimes;
        String strQuery = "SELECT "+DatabaseConstants.TIMETOADD_KEY_ROUTESTOPSTIME + ", " + DatabaseConstants.TIMETOADD_KEY_COMPANY +
                " FROM " + DatabaseConstants.TIMETOADD_TABLE + " INNER JOIN " + DatabaseConstants.ROUTES_TABLE + " ON " +
                DatabaseConstants.TIMETOADD_KEY_ROUTEID + "=" + DatabaseConstants.ROUTES_KEY_ROUTEID + " WHERE " + DatabaseConstants.TIMETOADD_KEY_ROUTEDIRECTION + "=" + mDirectionChanged;

        if (!result.getCompany().equals(DatabaseConstants.PARAM_ALL_COMPANIES)) {
            strQuery += " AND " + DatabaseConstants.TIMETOADD_KEY_COMPANY + "='" + result.getCompany() + "'";
        }
        Cursor cursor = mDatabase.rawQuery(strQuery, null);
        cursor.moveToFirst();
        String routeNameTmp = result.getRoute();

        allTimes = cursor.getString(0).split(",");

        if(mDirectionChanged == 1) {
            routeNameTmp = (translateName(result.getRoute()));
            addToStartTime = Integer.parseInt(allTimes[allTimes.length - busStopPositionInRoute(routeNameTmp, stop)- 1]) ;
        }
        else
            addToStartTime = Integer.parseInt(allTimes[busStopPositionInRoute(routeNameTmp, stop)]);
        cursor.close();
                //"Trasa: " + routeNameTmp + " przystanek: " + stop + " pozycja na liście: " + busStopPositionInRoute(routeNameTmp, stop));

        return addToStartTime;
    }

    public String translateName(String oldName){
        String newName;
        String[] splitingName = oldName.split("-");

        newName = splitingName[1] + "-" + splitingName[0];
        return newName;
    }

    public static ArrayList sortByValue(Map unsortMap) {
        List list = new LinkedList(unsortMap.entrySet());

        Collections.sort(list, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o1)).getValue())
                        .compareTo(((Map.Entry) (o2)).getValue());
            }
        });
        ArrayList<Object> sortedMap = new ArrayList<>();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            sortedMap.add(entry.getKey());
        }
        return sortedMap;
    }

}
