package com.apps.rdad.timetable.Utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Yoga on 2016-10-29.
 */

public class Utils {

    public static void showToast(Context context, String s) {
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }

}
