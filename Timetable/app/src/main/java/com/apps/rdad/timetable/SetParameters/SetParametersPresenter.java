package com.apps.rdad.timetable.SetParameters;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.Utils.Utils;


/**
 * Created by Yoga on 2016-09-24.
 */
public class SetParametersPresenter {

    SetParametersActivity mSetParametersActivity;
    RouteParameters routeParameters;
    public SetParametersPresenter(SetParametersActivity setParametersActivity) {
        this.mSetParametersActivity = setParametersActivity;
        routeParameters = new RouteParameters();

    }

    public void setTime(String time){
        routeParameters.setTime(time);
    }

    public void setDay(String day) {
        routeParameters.setWeekday(day);
    }

    public void setCompany(String company) {
        routeParameters.setCompany(company);
    }

    public void setStartStop(String startStop){
        routeParameters.setStartStop(startStop);
    }
    public void setEndStop(String endStop){
        routeParameters.setStopStop(endStop);
    }

    public void goToResults(String defaultWeekDay, String defaultCompany) {

        if (routeParameters.getWeekday()==null || routeParameters.getWeekday().isEmpty())
            routeParameters.setWeekday(defaultWeekDay);
        if (routeParameters.getCompany()==null || routeParameters.getCompany().isEmpty())
            routeParameters.setCompany(defaultCompany);
        if (routeParameters.getTime()==null || routeParameters.getTime().isEmpty()){
            routeParameters.setTime(mSetParametersActivity.getString(R.string.whole_day));
            mSetParametersActivity.checkWholeDay(true);
        }

        if (checkStartAndStop()) {
            mSetParametersActivity.goToResults(routeParameters);
        } else {
            Utils.showToast(mSetParametersActivity, mSetParametersActivity.getResources().getString(R.string.error_provide_start_stop));
        }

    }

    public boolean checkStartAndStop(){
        if(routeParameters.getStartStop()==null || routeParameters.getStopStop() == null)
            return false;

        return !routeParameters.getStartStop().isEmpty() && !routeParameters.getStopStop().isEmpty();

    }

    public void checkStops() {

        if(routeParameters.getStopStop()!=null && routeParameters.getStartStop()!=null){
            if(routeParameters.getStopStop().equals(routeParameters.getStartStop())){
                Utils.showToast(mSetParametersActivity, mSetParametersActivity.getResources().getString(R.string.error_toast_same_stops));
                mSetParametersActivity.setSearchEnabled(false);
            }else{
                mSetParametersActivity.setSearchEnabled(true);
            }
        }

    }
}
