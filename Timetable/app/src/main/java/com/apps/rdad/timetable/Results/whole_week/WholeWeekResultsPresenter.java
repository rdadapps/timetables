package com.apps.rdad.timetable.Results.whole_week;

import android.content.Context;
import android.database.SQLException;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.Results.ResultsContract;
import com.apps.rdad.timetable.Results.ResultsPresenter;
import com.apps.rdad.timetable.database.DatabaseHelper;
import com.apps.rdad.timetable.database.Result;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoga on 2017-02-19.
 */
public class WholeWeekResultsPresenter {
    private DatabaseHelper mDatabaseHelper;
    private List<Result> timetableList;
    private WholeWeekContract.OuterView mView;
    private Context mContext;

    public WholeWeekResultsPresenter(Context context, DatabaseHelper databaseHelper, WholeWeekContract.OuterView view) {
        this.mContext = context;
        this.mView = view;
        this.mDatabaseHelper = databaseHelper;
    }

    public void initDB() {
        try {
            mDatabaseHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error();
        }
        try {
            mDatabaseHelper.openDataBase();
        }catch(SQLException sqle){
            throw sqle;
        }
    }

    public List<Result> getTimetableList(String dayOfWeek, String company, String startLocation, String stopLocation, String departTime) throws ParseException {
        return mDatabaseHelper.getTimetableList(dayOfWeek, company, startLocation, stopLocation, departTime);

    }

}

