package com.apps.rdad.timetable.Constants;

/**
 * Created by Yoga on 2016-09-24.
 */
public class ExtraConstants {

    public static String STOP_LOCATION = "stop location";
    public static String SELECTED_DAYS = "selected days";
    public static String SELECTED_COMPANY = "selected company";
    public static String SELECTED_DEPARTTIME= "selected departTime";

}
