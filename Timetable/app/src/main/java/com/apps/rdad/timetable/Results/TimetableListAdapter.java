package com.apps.rdad.timetable.Results;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.database.Result;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yoga on 2016-10-03.
 */

public class TimetableListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Result> mTimetableList;
    private  Context context;
    private ItemViewHolder viewHolder;

    public TimetableListAdapter() {
        mTimetableList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.item_results, parent, false);
        viewHolder = new ItemViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int color = 0;
        Result result = mTimetableList.get(position);
        if(Integer.parseInt(result.getRoute())<0)
            viewHolder.mRoute.setText(R.string.no_time_left_label);
        else
            viewHolder.mRoute.setText(context.getString(R.string.time_left_label) + result.getRoute() + context.getString(R.string.min_suffix_label));

        viewHolder.mDepartureTime.setText(result.getDepartureTime());
        viewHolder.mArriveTime.setText(result.getArriveTime());
        viewHolder.mCompany.setText(result.getCompany());

        switch(result.getCompany()){
            case "Globus": {
                color = R.color.colorGlobus;
                break;
            }
            case "GołbaBus":{
                color = R.color.colorGolbaBus;
                break;
            }
            case "Makbus": {
                color = R.color.colorMakbus;
                break;
            }
            case "Bako":{
                color = R.color.colorBako;
                break;
            }
        }
        viewHolder.mCompany.setBackgroundColor(context.getResources().getColor(color));

    }

    @Override
    public int getItemCount() {
        return this.mTimetableList.size();
    }

    public void setResult(List<Result> result) {
        mTimetableList.addAll(result);
    }

    static class ItemViewHolder extends SectioningAdapter.ItemViewHolder {

        @BindView(R.id.tt_route)
        TextView mRoute;
        @BindView(R.id.tt_departure_time)
        TextView mDepartureTime;
        @BindView(R.id.tt_arrive_time)
        TextView mArriveTime;
        @BindView(R.id.tt_company)
        TextView mCompany;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

