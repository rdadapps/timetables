package com.apps.rdad.timetable.SetParameters;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Yoga on 2017-01-21.
 */

public class RouteParameters implements Parcelable{

    private String startStop;
    private String stopStop;
    private String mWeekday;
    private String time;
    private String company;
    protected RouteParameters() {
    }
    protected RouteParameters(Parcel in) {
        startStop = in.readString();
        stopStop = in.readString();
        mWeekday = in.readString();
        time = in.readString();
        company = in.readString();
    }

    public static final Creator<RouteParameters> CREATOR = new Creator<RouteParameters>() {
        @Override
        public RouteParameters createFromParcel(Parcel in) {
            return new RouteParameters(in);
        }

        @Override
        public RouteParameters[] newArray(int size) {
            return new RouteParameters[size];
        }
    };

    public String getStartStop() {
        return startStop;
    }

    public void setStartStop(String startStop) {
        this.startStop = startStop;
    }

    public String getWeekday() {
        return mWeekday;
    }

    public void setWeekday(String weekday) {
        mWeekday = weekday;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }


    public String getStopStop() {
        return stopStop;
    }

    public void setStopStop(String stopStop) {
        this.stopStop = stopStop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(startStop);
        parcel.writeString(stopStop);
        parcel.writeString(mWeekday);
        parcel.writeString(time);
        parcel.writeString(company);
    }
}
