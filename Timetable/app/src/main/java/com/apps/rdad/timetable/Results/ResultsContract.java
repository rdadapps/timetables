package com.apps.rdad.timetable.Results;

import com.apps.rdad.timetable.database.Result;

import java.util.ArrayList;

/**
 * Created by Yoga on 2017-02-19.
 */

public interface ResultsContract {
    interface View {
        void setEmptyList();
        void initAdapter(ArrayList<Result> results);
        void initWholeDayAdapter(ArrayList<Result> results);
    }
}
