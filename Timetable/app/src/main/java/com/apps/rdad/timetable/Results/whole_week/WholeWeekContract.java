package com.apps.rdad.timetable.Results.whole_week;

import android.support.v4.view.ViewPager;

import com.apps.rdad.timetable.SetParameters.RouteParameters;
import com.apps.rdad.timetable.database.Result;

import java.util.ArrayList;

/**
 * Created by Yoga on 2017-02-05.
 */

public interface WholeWeekContract {
    interface OuterView {
        void initData(RouteParameters routeParameters);
        void setupViewPager(ViewPager viewPager, boolean isWholeDay);
    }
    interface InnerViewPager {
        void setEmptyList();
        void initAdapter(ArrayList<Result> results);
        void initWholeDayAdapter(ArrayList<Result> results);
    }
}
