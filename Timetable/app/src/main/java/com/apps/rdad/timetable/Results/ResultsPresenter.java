package com.apps.rdad.timetable.Results;

import android.content.Context;
import android.database.SQLException;

import com.apps.rdad.timetable.R;
import com.apps.rdad.timetable.database.DatabaseHelper;
import com.apps.rdad.timetable.database.Result;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yoga on 2016-09-24.
 */
public class ResultsPresenter {
    private DatabaseHelper mDatabaseHelper;
    private List<Result> timetableList;
    private ResultsContract.View mView;
    private Context mContext;

    public ResultsPresenter(Context context, DatabaseHelper databaseHelper, ResultsContract.View view){
        this.mContext = context;
        this.mView = view;
        this.mDatabaseHelper = databaseHelper;
    }

    public void initDB() {
        try {
            mDatabaseHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error(); 
        }
        try {
            mDatabaseHelper.openDataBase();
        }catch(SQLException sqle){
            throw sqle;
        }
    }

    public void getTimetableList(String dayOfWeek, String company, String startLocation, String stopLocation, String departTime) throws ParseException {
        timetableList = mDatabaseHelper.getTimetableList(dayOfWeek, company, startLocation, stopLocation, departTime);
        if(timetableList==null||(timetableList!=null && timetableList.isEmpty())){
            mView.setEmptyList();
        }else {
            if (departTime.equals("") || departTime.equals(mContext.getString(R.string.whole_day_label)))
                mView.initWholeDayAdapter((ArrayList<Result>) timetableList);
            else
                mView.initAdapter((ArrayList<Result>) timetableList);
        }
    }
}