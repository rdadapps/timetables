package com.apps.rdad.timetable.database;

/**
 * Created by Yoga on 2016-11-26.
 */

public class MinutesToDepart {

    private int mMinutesStart;
    private int mMinutesStop;
    private String mCompany;

    public MinutesToDepart(int minutesStart, int minutesStop, String company) {
        mMinutesStart = minutesStart;
        mMinutesStop = minutesStop;
        mCompany = company;
    }

    public int getMinutesStart() {
        return mMinutesStart;
    }

    public void setMinutesStart(int minutesStart) {
        mMinutesStart = minutesStart;
    }

    public int getMinutesStop() {
        return mMinutesStop;
    }

    public void setMinutesStop(int minutesStop) {
        mMinutesStop = minutesStop;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }
}
