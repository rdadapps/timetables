package com.apps.rdad.timetable.database;

import com.apps.rdad.timetable.TimetableApplication;

/**
 * Created by Yoga on 2016-11-25.
 */

public class DatabaseConstants {


    public static String PARAM_ALL_WEEK = "Cały tydzień";
    public static String PARAM_ALL_COMPANIES = "Wszystkie";

    public static final String DB_NAME = "BusTimetable.sqlite";
    public static final String DB_PATH = TimetableApplication.getAppContext().getDatabasePath(DB_NAME).getAbsolutePath();


    public static final String ALLDATA_TABLE = "AllData";
    public static final String ALLDATA_KEY_ROUTE = "AllData.Route";
    public static final String ALLDATA_KEY_DEPARTTIME = "AllData.DepartTime";
    public static final String ALLDATA_KEY_COMPANY = "AllData.Company";
    public static final String ALLDATA_KEY_WEEKDAY = "AllData.WeekDay";

    public static final String ROUTES_TABLE = "Routes";
    public static final String ROUTES_KEY_ROUTEID = "Routes.RouteId";
    public static final String ROUTES_KEY_ROUTENAME = "Routes.RouteName";
    public static final String ROUTES_KEY_BUSSTOPS= "Routes.BusStops";

    public static final String TIMETOADD_TABLE= "TimeToAdd";
    public static final String TIMETOADD_KEY_ROUTEID= "TimeToAdd.RouteId";
    public static final String TIMETOADD_KEY_ROUTEDIRECTION= "TimeToAdd.RouteDirection";
    public static final String TIMETOADD_KEY_ROUTESTOPSTIME= "TimeToAdd.RouteStopsTime";
    public static final String TIMETOADD_KEY_COMPANY= "TimeToAdd.Company";

}
